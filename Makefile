all: sum_test

sum_test: sum.o main.o  
	gcc -o sum_test sum.o main.o

sum.o: sum.c sum.h
	gcc -c sum.c sum.h

main.o: main.c sum.h
	gcc -c main.c

clean:
	rm *.o sum_test

